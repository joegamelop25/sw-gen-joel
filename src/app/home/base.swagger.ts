export let SW = {
    'swagger': '2.0',
    'info': {
        'description': 'Descripcion',
        'version': '2.0.0',
        'title': 'APIM-customerAccounts-v2'
    },
    'host': 'apiinternaluat.entel.cl',
    'schemes': [
        'https'
    ],
    'definitions': {
        'ResponseError400': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 400
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'Peticion incorrecta'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'La peticion contiene sintaxis erronea y no deberÌa repetirse.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError401': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 401
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'No autorizado'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'Error en la entrega del token de autorizacion.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError404': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 404
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'No encontrado'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'Recurso no encontrado.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError405': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 405
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'Metodo no permitido'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'Una peticion fue hecha a una URI utilizando un mÈtodo de solicitud no soportado por dicha URI.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError422': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 422
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'Entidad no procesada'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'La peticion est· bien formada pero fue imposible seguirla debido a errores sem·nticos.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError429': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 429
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'Demasiadas solicitudes'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'Demasiadas solicitudes llegaron a la API.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        },
        'ResponseError500': {
            'type': 'object',
            'properties': {
                'result': {
                    'type': 'object',
                    'title': 'Objeto con el status de la llamada.',
                    'properties': {
                        'transactionId': {
                            'type': 'string',
                            'title': 'Id de transaccion generado por el API Management.',
                            'example': '0000016349eeab2d-2c9'
                        },
                        'code': {
                            'type': 'string',
                            'title': 'Codigo interno de respuesta.',
                            'example': 500
                        },
                        'description': {
                            'type': 'string',
                            'title': 'Descripcion interno de respuesta.',
                            'example': 'Error interno en el servidor'
                        },
                        'descriptionDetail': {
                            'type': 'string',
                            'title': 'Descripcion detallada o accion a realizar.',
                            'example': 'El servidor ha encontrado una error inesperado.'
                        },
                        'responseTimestamp': {
                            'type': 'string',
                            'title': 'Fecha y hora de respuesta del API Management.',
                            'example': '2018/05/10T17:49:56.080'
                        }
                    }
                }
            }
        }
    },
    'paths': {}
};

export const BASE_OBJECT_METHOD = {
    'tags': [
        'CustomerBills'
    ],
    'summary': 'whatever stuff',
    'description': 'whateber stuff',
    'consumes': [
        'application/json'
    ],
    'produces': [
        'application/json'
    ],
    'parameters': [
        {
            'name': 'applicationCode',
            'in': 'header',
            'description': 'Codigo ˙nico que representa al sistema consumidor.',
            'required': true,
            'type': 'string',
            'default': 'ATG'
        },
        {
            'name': 'consumerId',
            'in': 'header',
            'description': 'Codigo ˙nico de identificacion de la ejecucion, que identifica el evento del consumidor.',
            'required': true,
            'type': 'string',
            'default': '1234'
        },
        {
            'name': 'countryCode',
            'in': 'header',
            'description': 'Identifica el paÌs desde donde se origina la peticion.',
            'required': true,
            'type': 'string',
            'default': 'CHL'
        },
        {
            'name': 'requestTimestamp',
            'in': 'header',
            'description': 'Fecha, hora y timezone en el cual se envÌa la peticion del consumidor.',
            'required': true,
            'type': 'string',
            'default': '2018-12-12T12:12:12.444-03:00'
        },
        {
            'in': 'body',
            'name': 'body',
            'description': 'Estructura request a enviar',
            'required': true,
            'schema': {
                'type': 'object',
                'properties': {
                    'folio': {
                        'type': 'string'
                    },
                    'documentType': {
                        'type': 'string'
                    },
                    'listaPersonas': {
                        'type': 'object',
                        'properties': {
                            'persona': {
                                'type': 'array',
                                'items': {
                                    'properties': {
                                        'nombre': {
                                            'type': 'string'
                                        },
                                        'edad': {
                                            'type': 'integer'
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                'example': {
                    'folio': '',
                    'documentType': '',
                    'listaPersonas': {
                        'persona': [
                            {
                                'nombre': '',
                                'edad': ''
                            }
                        ]
                    }
                }
            }
        }
    ],
    'responses': {
        '200': {
            'description': 'OK - Procesamiento concluÌdo exitosamente',
            'schema': {
                'example': {
                    'result': {
                        'transactionId': '00000165d3b5f67c-122ba',
                        'code': '200',
                        'description': 'OK',
                        'descriptionDetail': 'Procesamiento concluÌdo exitosamente',
                        'responseTimestamp': '2018-09-24T12:08:48.518',
                        'ListaUsuarios': {
                            'usuario': [
                                {
                                    'nombre': '',
                                    'id': ''
                                }
                            ],
                            'ddd': '',
                            'a': [
                                {
                                    'aa': '',
                                    'ab': ''
                                }
                            ]
                        }
                    }
                },
                'type': 'object',
                'properties': {
                    'result': {
                        'type': 'object',
                        'title': 'The Result Schema',
                        'properties': {
                            'transactionId': {
                                'type': 'string',
                                'title': 'The Transactionid Schema',
                                'default': '',
                                'example': '00000165d3b5f67c-122ba',
                                'pattern': '^(.*)$'
                            },
                            'code': {
                                'type': 'string',
                                'title': 'The Code Schema',
                                'default': '',
                                'example': '200',
                                'pattern': '^(.*)$'
                            },
                            'description': {
                                'type': 'string',
                                'title': 'The Description Schema',
                                'default': '',
                                'example': 'OK',
                                'pattern': '^(.*)$'
                            },
                            'descriptionDetail': {
                                'type': 'string',
                                'title': 'The Descriptiondetail Schema',
                                'default': '',
                                'example': 'Procesamiento concluÌdo exitosamente',
                                'pattern': '^(.*)$'
                            },
                            'responseTimestamp': {
                                'type': 'string',
                                'title': 'The Responsetimestamp Schema',
                                'default': '',
                                'example': '2018-09-24T12:08:48.518',
                                'pattern': '^(.*)$'
                            },
                            'document': {
                                'type': 'array',
                                'title': 'The Document Schema',
                                'items': {
                                    'type': 'object',
                                    'properties': {
                                        'numberDocument': {
                                            'type': 'string',
                                            'title': 'The Numberdocument Schema',
                                            'default': '',
                                            'example': '',
                                            'pattern': '^(.*)$'
                                        }
                                    }
                                }
                            }
                        }
                    },
                    'ListaUsuarios': {
                        'type': 'object',
                        'properties': {
                            'usuario': {
                                'type': 'array',
                                'items': {
                                    'properties': {
                                        'nombre': {
                                            'type': 'string'
                                        },
                                        'id': {
                                            'type': 'number'
                                        }
                                    }
                                }
                            },
                            'ddd': {
                                'type': 'string'
                            },
                            'a': {
                                'type': 'array',
                                'items': {
                                    'properties': {
                                        'aa': {
                                            'type': 'string'
                                        },
                                        'ab': {
                                            'type': 'string'
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        },
        '400': {
            'description': 'Error - Par·metros invalidos',
            'schema': {
                '$ref': '#/definitions/ResponseError400'
            }
        },
        '401': {
            'description': 'Error - Error de autenticacion',
            'schema': {
                '$ref': '#/definitions/ResponseError401'
            }
        },
        '404': {
            'description': 'Error - Recurso no encontrado.',
            'schema': {
                '$ref': '#/definitions/ResponseError404'
            }
        },
        '405': {
            'description': 'Error - MÈtodo no permitido',
            'schema': {
                '$ref': '#/definitions/ResponseError405'
            }
        },
        '422': {
            'description': 'Error - Error de procesamiento de la solicitud',
            'schema': {
                '$ref': '#/definitions/ResponseError422'
            }
        },
        '429': {
            'description': 'Error - LÌmite de solicitudes excedido',
            'schema': {
                '$ref': '#/definitions/ResponseError429'
            }
        },
        '500': {
            'description': 'Error - Error inesperado en el servidor',
            'schema': {
                '$ref': '#/definitions/ResponseError500'
            }
        }
    }
};
