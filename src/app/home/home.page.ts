import {ChangeDetectorRef, Component} from '@angular/core';
import { ToastController } from '@ionic/angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { SW, BASE_OBJECT_METHOD } from './base.swagger';
import * as yamls from 'js-yaml';
import { Router } from '@angular/router';
import { ParamsService } from '../utils/params.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {

  public todo: FormGroup;
  public methods: Object = {};
  public fileForm: FormGroup;
  public swg: any = SW;
  constructor( private formBuilder: FormBuilder,
               public router: Router,
               public params: ParamsService,
               public toastController: ToastController,
               private cd: ChangeDetectorRef) {
    this.fileForm = this.formBuilder.group({
      json: [null, Validators.required]
    });
    this.todo = this.formBuilder.group({
      description: ['this is description', Validators.required],
      version: ['2.0.0'],
      nameApi: ['name api'],
      host: ['apiinternaluat.entel.cl'],
      schema: ['https'],
      envUri: ['internal'],
      path: ['/customer/v2/customerBills/invoice', Validators.required],
      methodName: ['CustomerBills', Validators.required],
      methodType: ['get', Validators.required],
      resume: ['the proposal of this API is to get the accounts state'],
      format: ['json']
    });
  }
  chargeJson = (event) => {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.fileForm.patchValue({
          json: reader.result
        });
        this.cd.markForCheck();
        this.readTextFile(this.fileForm.value.json);
      };
    }
  }
  readTextFile = (file) => {
    const rawFile = new XMLHttpRequest();
    rawFile.open('GET', file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          this.swg = JSON.parse(rawFile.responseText);
          this.todo.setValue({
            description: this.swg.info.description || this.todo.value['description'],
            version: this.swg.info.version || this.todo.value['version'],
            nameApi: this.swg.info.title || this.todo.value['nameApi'],
            host: this.swg.host || this.todo.value['host'],
            schema: 'https',
            envUri: 'internal',
            path: '/customer/v2/customerBills/invoice',
            methodName: 'CustomerBills',
            methodType: 'get',
            resume: 'the proposal of this API is to get the accounts state',
            format: 'json'
          });
          this.methods = this.swg['paths'];
          localStorage.removeItem('methods');
          localStorage.setItem('methods', JSON.stringify(this.methods));
        }
      }
    };
    rawFile.send(null);
  }
  ionViewDidEnter = () => {
    const values = JSON.parse(JSON.stringify(localStorage.getItem('methods')));
    this.methods = (typeof values === 'string' || values === null) ? JSON.parse(values) : {} ;
  }
  addMethod = () => {
    const path = this.todo.value['path'];
    const methodType = this.todo.value['methodType'];
    this.methods = this.methods || {};
    this.methods[path] = this.methods[path] || {};
    this.methods[path][methodType] = this.methods[path][methodType] || BASE_OBJECT_METHOD;
    localStorage.setItem('methods', JSON.stringify(this.methods));
  }
  deleteMethod = (path, method) => {
    delete this.methods[path][method];
    localStorage.removeItem('methods');
    localStorage.setItem('methods', JSON.stringify(this.methods));
  }
  editMethod = (path, method) => {
    this.params.set({ path: path, method: method, description: this.methods[path][method]['description'] });
    this.router.navigate(['/edit-method']);
  }
  clearAll = () => {
    this.methods = {};
    localStorage.removeItem('methods');
  }
  logForm = async () => {
    this.swg.info.description = this.todo.value['description'];
    this.swg.info.version = this.todo.value['version'];
    this.swg.info.title = this.todo.value['nameApi'];
    this.swg.host = this.todo.value['host'];
    this.swg.paths = JSON.parse(localStorage.getItem('methods'));
    if (!this.swg.schemes.includes(this.todo.value['schema'])) {
      this.swg.schemes.push(this.todo.value['schema']);
    }
    const result = this.todo.value['format'] === 'json' ? JSON.stringify(this.swg, null, '\t') : this.yaml(this.swg);
    this.copyToClipboard(result);
    const toast = await this.toastController.create({
      message:  this.todo.value['format'].toUpperCase() + ' Copied in clipboard',
      duration: 1000
    });
    toast.present();
  }
  yaml = (jj: any): any => {
    const options = {
      flowLevel: -1,
      noRefs: true,
      schema: yamls.JSON_SCHEMA,
      lineWidth: 1000000
    };
    return yamls.safeDump(jj, options);
  }
  copyToClipboard = (str: any): any => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    document.body.appendChild(el);
    const selected =
        document.getSelection().rangeCount > 0
            ? document.getSelection().getRangeAt(0)
            : false;
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    if (selected) {
      document.getSelection().removeAllRanges();
      document.getSelection().addRange(selected);
    }
  }
}
