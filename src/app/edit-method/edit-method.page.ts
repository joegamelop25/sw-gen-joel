import {ChangeDetectorRef, Component} from '@angular/core';
import {NavController, ToastController} from '@ionic/angular';
import {ParamsService} from '../utils/params.service';
import * as xmlparse from 'xml-js';
import {MdEditorOption, UploadResult} from '../md/md.types';
import {Observable, throwError} from 'rxjs';
import {FormBuilder, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {throws} from 'assert';

@Component({
    selector: 'app-edit-method',
    templateUrl: './edit-method.page.html',
    styleUrls: ['./edit-method.page.scss'],
})
export class EditMethodPage {
    public data: any;
    public file: any;
    public fileJson: any;
    public rawFile: any;
    public _READER = new FileReader();
    public attrs: object = {};
    public flag = 0;
    public filesForm: any = this.formBuilder.group({
        file: [null, Validators.required],
        io: ['file', Validators.required],
        uri: ['']
    });

    constructor(public navCtrl: NavController,
                public params: ParamsService,
                public toastController: ToastController,
                private formBuilder: FormBuilder,
                private cd: ChangeDetectorRef,
                public http: HttpClient) {
        this.data = this.params.get();
        this.content = this.data['description'];
    }

    public options: MdEditorOption = {
        enablePreviewContentClick: false,
        markedjsOpt: {
            tables: true,
            gfm: true
        }
    };
    public content: string;
    public mode = 'editor';
    onURIChange = (event) => {
       // #TODO: make request to api
    }
    onSubmit = () => {
        console.log('submitted');
    }
    onFileChange = (event) => {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = () => {
                this.filesForm.patchValue({
                    file: reader.result
                });
                this.cd.markForCheck();
                console.log(this.filesForm);
                this.readTextFile(this.filesForm.value.file);
            };
        }
    }
    readTextFile = (file) => {
        const rawFile = new XMLHttpRequest();
        rawFile.open('GET', file, false);
        rawFile.onreadystatechange = async () => {
            if (rawFile.readyState === 4) {
                if (rawFile.status === 200 || rawFile.status === 0) {
                    const allText = rawFile.responseText;
                    const objxsd = xmlparse.xml2js(allText, {compact: false});
                    console.log(objxsd);
                    const complex: any[] = objxsd['elements'][0]['elements'];
                    console.log(complex);
                    const complexArr: any[] = [];
                    await complex.map((value: any, key: any) => {
                        if (value['name'] === 'xs:complexType') {
                            complexArr.push(complex[key]);
                        }
                    });
                    const req = [], rsp = [];
                    await complexArr.map((value: any, key: any) => {
                        if (value['attributes']['name'].includes('_REQ_Type')) {
                            req.push(value['elements']);
                        }
                        if (value['attributes']['name'].includes('_RSP_Type')) {
                            rsp.push(value['elements']);
                        }
                    });
                    console.log(req, rsp);
                    console.log(complexArr);
                    this.recursiveXsd(req, false)
                        .then(() => {
                            console.log(this.attrs, 'exit with success');
                        })
                        .catch((e) => {
                            console.log(e);
                        });
                }
            }
        };
        rawFile.send(null);
    }
    recursiveXsd = async (args: any[], prev: any): Promise<void> => {
        const pr = prev;
        try {
            await args.map((value: any, key: any) => {
                if (Array.isArray(value)) {
                    this.recursiveXsd(value, false);
                } else  if (value['attributes']) {
                    if (typeof value['attributes']['name'] === 'string') {
                            if (value['attributes']['name'] === 'Body') {
                                this.flag++;
                            }
                            if (this.flag > 0 && value['attributes']['name'] !== 'Body') {
                                console.log(this.attrs[pr], typeof this.attrs[pr]);
                                if (pr) {
                                    this.attrs[pr][value['attributes']['name']] = {};
                                } else {
                                    this.attrs[value['attributes']['name']] = {};
                                }
                                console.log('attached');
                            }
                        if (Array.isArray(value['elements'])) {
                            this.recursiveXsd(value['elements'], value['attributes']['name'] || false);
                            console.log('sent name', value['attributes']['name']);
                        }
                    }
                } else if (Array.isArray(value['elements'])) {
                    this.recursiveXsd(value['elements'], value['attributes']['name'] || false);
                } else {
                    console.log(value, 'no array, no attrs, no elements array');
                }
            });
        } catch (e) {
            throw e;
        }
    }
    changeFile = (e) => {
        try {
            console.log(this.rawFile.replace(/^.*\\/, ''));
        } catch (e) {
            console.log(e, 'err');
        }
        this
            .handleImageSelection(this.rawFile.replace(/^.*\\/, ''))
            .subscribe((res) => {
                    console.log(res);
                },
                (error) => {
                    console.dir(error);
                },
                () => {
                    console.log('it is completed');
                });
    }
    handleImageSelection = (event: any): Observable<any> => {
        this._READER.readAsDataURL(event);
        return Observable.create((observer) => {
            this._READER.onloadend = () => {
                observer.next(this._READER.result);
                observer.complete();
            };
        });
    }
    dismiss = () => {
        this.navCtrl.goBack({
            animated: true,
            animationDirection: 'back'
        });
    }
    save = async () => {
        const values = JSON.parse(localStorage.getItem('methods'));
        await Object.keys(values).map((key: any) => {
            if (key === this.data['path']) {
                values[key][this.data['method']]['description'] = this.content;
                localStorage.removeItem('methods');
                localStorage.setItem('methods', JSON.stringify(values));
            }
        });
        const toast = await this.toastController.create({
            message: 'Description saved',
            duration: 1000
        });
        toast.present();
    }

    changeMode = () => {
        if (this.mode === 'editor') {
            this.mode = 'preview';
        } else {
            this.mode = 'editor';
        }
    }

    togglePreviewClick = () => {
        this.options.enablePreviewContentClick = !this.options.enablePreviewContentClick;
        this.options = Object.assign({}, this.options);
    }

    doUpload = (files: Array<File>): Promise<Array<UploadResult>> => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                const result: Array<UploadResult> = [];
                for (const file of files) {
                    result.push({
                        name: file.name,
                        url: `https://avatars3.githubusercontent.com/${file.name}`,
                        isImg: file.type.indexOf('image') !== -1
                    });
                }
                resolve(result);
            }, 3000);
        });
    }
}
